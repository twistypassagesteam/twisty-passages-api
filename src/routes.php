<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use TwistyPassagesApi\Middleware\ContentTypeChecker;

return function (App $app) {
    $container = $app->getContainer();

    $app->get('/', function (Request $request, Response $response, array $args) use ($container) {
        $container->get('logger')->info("TPA '/' route");
        // Render index view
        return $container->get('renderer')->render($response, 'index.phtml', $args);
    });

    $app->post('/v1/register', 'TwistyPassagesApi\Action\RegisterAction:register')
        ->add($container->get(ContentTypeChecker::class));

    $app->get('/v1/confirm', 'TwistyPassagesApi\Action\RegisterAction:confirm');

    $app->post('/v1/login', 'TwistyPassagesApi\Action\LoginAction:login')
        ->add($container->get(ContentTypeChecker::class));

};
