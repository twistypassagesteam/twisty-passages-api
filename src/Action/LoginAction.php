<?php

namespace TwistyPassagesApi\Action;

use Exception;
use Slim\Http\Request as Request;
use Slim\Http\Response as Response;
use TwistyPassagesApi\Service\LoginService;

class LoginAction
{

    /**
     * @var LoginService
     */
    protected $loginService;


    public function __construct($loginService)
    {
        $this->loginService = $loginService;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     * @throws Exception
     */
    public function login(Request $request, Response $response, $args)
    {
        try {
            $parsedBody = $request->getParsedBody();
            $user = $this->loginService->login($parsedBody);
            return $response->withJson(['token' => $user->getToken()]);
        }
        catch (Exception $e) {
            return $response->withStatus($e->getCode())->withJson(['message' => $e->getMessage()]);
        }
    }

}
