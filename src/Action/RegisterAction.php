<?php

namespace TwistyPassagesApi\Action;

use Exception;
use Slim\Http\Request as Request;
use Slim\Http\Response as Response;
use Slim\Views\PhpRenderer;
use TwistyPassagesApi\Entity\User;
use TwistyPassagesApi\Service\RegisterService;

class RegisterAction
{

    /**
     * @var PhpRenderer
     */
    protected $renderer;

    /**
     * @var RegisterService
     */
    protected $registerService;


    public function __construct(PhpRenderer $renderer, RegisterService $registerService)
    {
        $this->renderer = $renderer;
        $this->registerService = $registerService;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     * @throws Exception
     */
    public function register(Request $request, Response $response, $args)
    {
        try {
            $parsedBody = $request->getParsedBody();
            $this->registerService->validatePostData($parsedBody);
            if (!empty($this->registerService->getErrors())) {
                return $response->withStatus(400)->withJson($this->registerService->getErrors());
            }
            $user = $this->registerService->create($parsedBody);
            if (!$user instanceof User) {
                throw new Exception('User could not be created', 500);
            }
            $this->registerService->sendConfirmationMail($user);
            return $response->withStatus(201)->withJson($user);
        }
        catch (Exception $e) {
            throw $e;
        }
    }

    public function confirm(Request $request, Response $response, $args)
    {
        try {
            $queryParams = $request->getQueryParams();
            $this->registerService->processConfirmation($queryParams);
            return $this->renderer->render($response, 'confirm.phtml');
        }
        catch (Exception $e) {
            throw $e;
        }
    }

}
