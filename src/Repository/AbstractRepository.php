<?php

namespace TwistyPassagesApi\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;

abstract class AbstractRepository extends EntityRepository
{

    public function __construct(EntityManagerInterface $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
    }

    public function persist($entity)
    {
        $this->_em->persist($entity);
        return $this;
    }

    public function flush($entity = null)
    {
        $this->_em->flush($entity);
        return $this;
    }

}
