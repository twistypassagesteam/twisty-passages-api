<?php

namespace TwistyPassagesApi\Middleware;

use Exception;
use Slim\Http\Request;
use Slim\Http\Response;

class ContentTypeChecker
{

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     * @return Response
     * @throws Exception
     */
    public function __invoke(Request $request, Response $response, $next)
    {

        $contentType = strtolower($request->getContentType());
        if (strpos($contentType, 'application/json') === false) {
            throw new Exception("unsupported content type – only accept application/json", 415);
        }
        if (strpos($contentType, 'charset') !== false && strpos($contentType, 'utf-8') === false) {
            throw new Exception("unsupported content type – only accept UTF-8 charset", 415);
        }
        return $next($request, $response);
    }

}
