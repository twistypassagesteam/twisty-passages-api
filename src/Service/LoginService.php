<?php

namespace TwistyPassagesApi\Service;

use DateTime;
use Exception;
use TwistyPassagesApi\Entity\User;
use TwistyPassagesApi\Helper\ToolProvider;
use TwistyPassagesApi\Repository\UserRepository;

class LoginService
{

    protected $userRepo;

    protected $errors;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
        $this->errors = [];
    }

    public function login($parsedBody)
    {
        if (!array_key_exists('username', $parsedBody)) {
            throw new Exception('Unable to log in.', 400);
        }
        if (!array_key_exists('password', $parsedBody)) {
            throw new Exception('Unable to log in.', 400);
        }
        $username = filter_var($parsedBody['username'], FILTER_SANITIZE_STRING);
        $password = filter_var($parsedBody['password'], FILTER_SANITIZE_STRING);
        $user = $this->userRepo->findOneBy(['username' => $username]);
        if (!$user instanceof User) {
            throw new Exception('Unable to log in.', 400);
        }
        if (!password_verify($password, $user->getPassword())) {
            throw new Exception('Invalid login information.', 400);
        }
        if ($user->getAuthToken() !== null) {
            throw new Exception('Account has not been confirmed yet.', 400);
        }
        $user->setToken(ToolProvider::createToken());
        $user->setModifiedAt(new DateTime());
        return $user;
    }

}
