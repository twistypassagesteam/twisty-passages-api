<?php

namespace TwistyPassagesApi\Service;

use DateTime;
use Exception;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validation;
use TwistyPassagesApi\Entity\User;
use TwistyPassagesApi\Helper\ToolProvider;
use TwistyPassagesApi\Repository\UserRepository;

class RegisterService
{

    const MAILTEXT_CONFIRM = 'confirm';

    protected $userRepo;

    protected $mailer;

    protected $errors;

    public function __construct(UserRepository $userRepo, TwistyMailer $mailer)
    {
        $this->userRepo = $userRepo;
        $this->mailer = $mailer;
        $this->errors = [];
    }

    public function validatePostData($postData)
    {
        $this->errors = [];
        $validator = Validation::createValidator();
        $username = strtolower($postData['username']);
        $violations = $validator->validate($username, [
            new Length(['min' => 3, 'max' => 32]),
            new Type('alnum'),
            new NotBlank(),
        ]);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                $this->errors['username'] = [
                    'message' => $violation->getMessage()
                ];
            }
        }
        if ($this->userRepo->findOneBy(['username' => $username]) !== null) {
            $this->errors['username'] = [
                'message' => 'That username is already taken.'
            ];
        }
        $password = $postData['password'];
        $violations = $validator->validate($password, [
            new Length(['min' => 8, 'max' => 32]),
            new Type('alnum'),
            new NotBlank()
        ]);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                $this->errors['password'] = [
                    'message' => $violation->getMessage()
                ];
            }
        }
        $email = strtolower($postData['email']);
        $violations = $validator->validate($email, [
            new Length(['min' => 6, 'max' => 32]),
            new Email(),
            new NotBlank()
        ]);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                $this->errors['email'] = [
                    'message' => $violation->getMessage()
                ];
            }
        }
        if ($this->userRepo->findOneBy(['email' => $email]) !== null) {
            $this->errors['email'] = [
                'message' => 'That e-mail is already registered.'
            ];
        }
    }

    public function create($postData)
    {
        $username = strtolower($postData['username']);
        $password = $postData['password'];
        $email = strtolower($postData['email']);
        $pwHash = password_hash($password, PASSWORD_BCRYPT, ['cost' => 11]);
        $user = new User();
        $now = new DateTime();
        $user->setUserLevel(1)
            ->setAuthToken(ToolProvider::createToken(32))
            ->setToken(null)
            ->setCreatedAt($now)
            ->setModifiedAt($now)
            ->setPassword($pwHash)
            ->setEmail($email)
            ->setUsername($username);
        $this->userRepo->persist($user)->flush($user);
        return $user;
    }

    public function sendConfirmationMail(User $user)
    {
        $this->mailer->addAddress($user->getEmail());
        $this->mailer->Subject = 'Twisty Passages User Registration';
        $text = $this->mailer->replaceTextVariable(self::MAILTEXT_CONFIRM, ['[[USERNAME]]' => $user->getUsername(), '[[TOKEN]]' => $user->getAuthToken()]);
        $this->mailer->Body = $text;
        $this->mailer->AltBody = $text;
        $this->mailer->send();
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function processConfirmation($queryParams)
    {
        $valid = true;
        if (!array_key_exists('user', $queryParams)) {
            $valid = false;
        }
        if (!array_key_exists('token', $queryParams)) {
            $valid = false;
        }
        if (!$valid) {
            throw new Exception('Invalid confirmation request!', 400);
        }
        $user = $this->userRepo->findOneBy([
            'username' => filter_var($queryParams['user'], FILTER_SANITIZE_STRING),
            'authToken' => filter_var($queryParams['token'], FILTER_SANITIZE_STRING),
        ]);
        if (!$user instanceof User) {
            throw new Exception('Invalid confirmation request!', 400);
        }
        $user->setAuthToken(null);
        $user->setModifiedAt(new DateTime());
        $this->userRepo->flush($user);
    }

}
