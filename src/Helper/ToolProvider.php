<?php

namespace TwistyPassagesApi\Helper;

class ToolProvider
{

    static public function createToken($length = 64)
    {
        $token = bin2hex(random_bytes($length));
        return $token;
    }

}
