<?php

namespace TwistyPassagesApi\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass="TwistyPassagesApi\Repository\UserRepository")
 * @ORM\Table(name="User")
 */
class User implements JsonSerializable
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="username", type="string", length=64, unique=true)
     * @var string
     */
    protected $username;

    /**
     * @ORM\Column(name="email", type="string", length=64, unique=true)
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(name="password", type="string")
     * @var string
     */
    protected $password;

    /**
     * @ORM\Column(name="token", type="string", length=128, nullable=true)
     * @var string
     */
    protected $token;

    /**
     * @ORM\Column(name="auth_token", type="string", length=128, nullable=true)
     * @var string
     */
    protected $authToken;

    /**
     * @ORM\Column(name="user_level", type="integer", options={"unsigned": true,"default": 1})
     * @var int
     */
    protected $userLevel;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     * @var DateTime
     */
    protected $modifiedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): User
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param null|string $token
     * @return User
     */
    public function setToken($token): User
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthToken()
    {
        return $this->authToken;
    }

    /**
     * @param string|null $authToken
     * @return User
     */
    public function setAuthToken($authToken): User
    {
        $this->authToken = $authToken;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserLevel(): int
    {
        return $this->userLevel;
    }

    /**
     * @param int $userLevel
     * @return User
     */
    public function setUserLevel(int $userLevel): User
    {
        $this->userLevel = $userLevel;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return User
     */
    public function setCreatedAt(DateTime $createdAt): User
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getModifiedAt(): DateTime
    {
        return $this->modifiedAt;
    }

    /**
     * @param DateTime $modifiedAt
     * @return User
     */
    public function setModifiedAt(DateTime $modifiedAt): User
    {
        $this->modifiedAt = $modifiedAt;
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'username' => $this->getUsername(),
            'user_level' => $this->getUserLevel(),
            'created_at' => $this->getCreatedAt()->format(DateTime::ATOM),
            'modified_at' => $this->getModifiedAt()->format(DateTime::ATOM)
        ];
    }
}
