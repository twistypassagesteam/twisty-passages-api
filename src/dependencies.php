<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Views\PhpRenderer;
use TwistyPassagesApi\Action\LoginAction;
use TwistyPassagesApi\Action\RegisterAction;
use TwistyPassagesApi\Entity\User;
use TwistyPassagesApi\Handler\TwistyErrorHandler;
use TwistyPassagesApi\Middleware\ContentTypeChecker;
use TwistyPassagesApi\Service\LoginService;
use TwistyPassagesApi\Service\RegisterService;
use TwistyPassagesApi\Service\TwistyMailer;

return function (App $app) {
    $container = $app->getContainer();

    // view renderer
    $container['renderer'] = function (ContainerInterface $c) {
        $settings = $c->get('settings')['renderer'];
        return new PhpRenderer($settings['template_path']);
    };

    // monolog
    $container['logger'] = function (ContainerInterface $c) {
        $settings = $c->get('settings')['logger'];
        $logger = new Logger($settings['name']);
        $logger->pushProcessor(new UidProcessor());
        $logger->pushHandler(new StreamHandler($settings['path'], $settings['level']));
        return $logger;
    };

    $container[EntityManager::class] = function (ContainerInterface $c) {
        $settings = $c->get('settings');
        $config = Setup::createAnnotationMetadataConfiguration(
            $settings['doctrine']['meta']['entity_path'],
            $settings['doctrine']['meta']['auto_generate_proxies'],
            $settings['doctrine']['meta']['proxy_dir'],
            $settings['doctrine']['meta']['cache'],
            false
        );
        $em = EntityManager::create($settings['doctrine']['connection'], $config);
        $em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        return $em;
    };

    $container[TwistyMailer::class] = function (ContainerInterface $c): TwistyMailer {
        $settings = $c->get('settings')['phpmailer'];
        return new TwistyMailer($settings);
    };

    $container[ContentTypeChecker::class] = function (ContainerInterface $c): ContentTypeChecker {
        return new ContentTypeChecker();
    };

    $container['errorHandler'] = function (ContainerInterface $c): TwistyErrorHandler {
        $settings = $c->get('settings');
        return new TwistyErrorHandler(
            $c->get('logger'),
            $c->get(TwistyMailer::class),
            $settings['displayErrorDetails']
        );
    };

    $container[RegisterService::class] = function (ContainerInterface $c): RegisterService {
        $settings = $c->get('settings');
        $em = $c->get(EntityManager::class);
        $mailer = new TwistyMailer($settings['phpmailer']);
        return new RegisterService($em->getRepository(User::class), $mailer);
    };

    $container[RegisterAction::class] = function (ContainerInterface $c): RegisterAction {
        return new RegisterAction($c->get('renderer'), $c->get(RegisterService::class));
    };

    $container[LoginService::class] = function (ContainerInterface $c): LoginService {
        $em = $c->get(EntityManager::class);
        return new LoginService($em->getRepository(User::class));
    };

    $container[LoginAction::class] = function (ContainerInterface $c): LoginAction {
        return new LoginAction($c->get(LoginService::class));
    };

};
