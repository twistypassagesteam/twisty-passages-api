<?php

use Monolog\Logger;
use TwistyPassagesApi\Service\TwistyMailer;

define('APP_ROOT', __DIR__. "/..");

return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => APP_ROOT . '/templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'tpa',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : APP_ROOT . '/logs/app.log',
            'level' => Logger::DEBUG,
        ],

        // Doctrine settings
        'doctrine' => [
            'meta' => [
                'entity_path' => [
                    'src/Entity',
                ],
                'auto_generate_proxies' => true,
                'proxy_dir' =>  __DIR__.'/../var/doctrine',
                'cache' => null,
            ],
            'connection' => [

                'driver'   => 'pdo_mysql',
                'host'     => 'localhost',
                'dbname'   => 'tp',
                'user'     => 'tp',
                'password' => 'twistypassages',
                'charset'  => 'utf8',

                'driverOptions' => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                ]

            ]
        ],

        // mailer config
        'phpmailer' => [
            TwistyMailer::EXCEPTIONS => false,
            'noMailings' => false,
            'isSmtp' => true,
            'defaults' => [
                'From' => 'twisty.passages@totalmadownage.com',
                'FromName' => 'twisty.passages',
                'isHtml' => true,
                'errorAddress' => 'gevrik@totalmadownage.com',
                'errorName' => 'twisty-passages-api',
                'errorSubject' => 'tpa - errors',
                'defaultCc' => null,
                'defaultBcc' => null,
                'smtpData' => [
                    'smtpDebug' => 2,
                    'smtpAuth' => true,
                    'smtpSecure' => 'tls',
                    'mainMailer' => 'ssl0.ovh.net',
                    'backupMailer' => 'ssl0.ovh.net',
                    'username' => 'twisty.passages@totalmadownage.com',
                    'password' => 'secret',
                    'smtpPort' => 587,
                ],
            ],
            'mailTexts' => [
                'confirm' => nl2br(file_get_contents(getcwd(). '/../data/text/registration_confirmation_mail.txt'))
            ]
        ],
    ],
];
