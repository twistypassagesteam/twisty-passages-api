<?php

namespace TwistyPassagesApi\Handler;

use Monolog\Logger;
use PHPMailer\PHPMailer\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Handlers\Error;
use Slim\Http\Body;
use TwistyPassagesApi\Service\TwistyMailer;
use UnexpectedValueException;

final class TwistyErrorHandler extends Error
{

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var TwistyMailer
     */
    protected $mailer;


    /**
     * TwistyErrorHandler constructor.
     * @param Logger $logger
     * @param TwistyMailer $mailer
     * @param bool $displayErrorDetails
     */
    public function __construct(
        Logger $logger,
        TwistyMailer $mailer,
        $displayErrorDetails = false
    )
    {
        parent::__construct($displayErrorDetails);
        $this->logger = $logger;
        $this->mailer = $mailer;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param \Exception $exception
     * @return Response
     */
    public function __invoke(Request $request, Response $response, \Exception $exception)
    {
        $code = $exception->getCode();
        $message = $exception->getMessage();
        $errorSummary = sprintf("=== API ERROR === code: %s message: %s", $code, $message);
        $this->logger->error($errorSummary);
        $stackBodyHtml = sprintf('<h2>%s</h2>', $errorSummary);
        if ($this->displayErrorDetails) {
            $stackBody = "=== ERROR STACK ===";
            $stackBodyHtml .= sprintf("<h3>%s</h3>", $stackBody);
            $this->logger->error($stackBody);
            $traceArray = explode("\n", $exception->getTraceAsString());
            foreach ($traceArray as $traceMessage) {
                $this->logger->error($traceMessage);
                $stackBodyHtml .= sprintf('<p>%s</p>', $traceMessage);
            }
        }
        if ($this->mailer && $this->mailer->isMailer() === true) {
            $this->mailer->Subject = $errorSummary;
            $this->mailer->Body = $stackBodyHtml;
            $this->mailer->AltBody = $this->mailer->html2text($stackBodyHtml);
            try {
                $this->mailer->send();
            } catch (Exception $e) {
                $this->logger->error(sprintf('failed sending mail: %s', $e->getMessage()));
            }
        }
        $contentType = $this->determineContentType($request);
        switch ($contentType) {
            case 'application/json':
                list($code, $output) = $this->renderJsonErrorMessage($exception);
                break;
            case 'text/html':
                list($code, $output) = $this->renderHtmlErrorMessage($exception);
                break;
            default:
                throw new UnexpectedValueException('Cannot render unknown content type ' . $contentType);
        }

        $this->writeToErrorLog($exception);

        $body = new Body(fopen('php://temp', 'r+'));
        $body->write($output);

        return $response
            ->withStatus($code)
            ->withHeader('Content-Type', $contentType)
            ->withBody($body);
    }

    /**
     * Render JSON error
     *
     * @param \Exception $exception
     *
     * @return array
     */
    protected function renderJsonErrorMessage(\Exception $exception)
    {
        $code = $exception->getCode();
        switch ($code) {
            default:
                if ($this->displayErrorDetails) {
                    $code = 500;
                    $error = ['message' => $exception->getMessage()];
                }
                else {
                    $code = 500;
                    $error = ['message' => 'something went wrong - please try again later'];
                }
                break;
            case 0:
                if (strpos($exception->getMessage(), 'Integrity constraint violation')) {
                    if ($this->displayErrorDetails) {
                        $code = 500;
                        $error = ['message' => $exception->getMessage()];
                    }
                    else {
                        $code = 409;
                        $error = ['message' => 'resource already exists'];
                    }
                }
                else {
                    if ($this->displayErrorDetails) {
                        $code = 500;
                        $error = ['message' => $exception->getMessage()];
                    }
                    else {
                        $code = 500;
                        $error = ['message' => 'something went wrong - please try again later'];
                    }
                }
                break;
            case 404:
                $error = ['message' => $exception->getMessage()];
                break;
            case 400:
                $error = ['message' => ($exception->getMessage()) ? $exception->getMessage() : 'bad request'];
                break;
            case 401:
                $error = ['message' => 'unauthorized'];
                break;
            case 409:
                $error = ['message' => 'resource already exists'];
                break;
            case 415:
                $error = ['message' => 'unsupported content type – only accept application/json'];
                break;
            case 422:
                $messageArray = json_decode($exception->getMessage(), true);
                $error = ['message' => json_encode($messageArray)];
                break;
        }

        if ($this->displayErrorDetails) {
            $error['exception'] = [];
            do {
                $error['exception'][] = [
                    'type' => get_class($exception),
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'trace' => explode("\n", $exception->getTraceAsString()),
                ];
            } while ($exception = $exception->getPrevious());
        }

        return [$code, json_encode($error, JSON_PRETTY_PRINT)];
    }

    /**
     * Render HTML error page
     *
     * @param  \Exception $exception
     *
     * @return array
     */
    protected function renderHtmlErrorMessage(\Exception $exception)
    {
        $code = $exception->getCode();
        $detailedMessage = null;
        switch ($code) {
            default:
                $code = 500;
                $title = 'something went wrong - please try again later';
                break;
            case 0:
                if (strpos($exception->getMessage(), 'Integrity constraint violation')) {
                    $code = 409;
                    $title = 'resource already exists';
                }
                else {
                    $code = 500;
                    $title = 'something went wrong - please try again later';
                }
                break;
            case 400:
                $title = 'bad request';
                $detailedMessage = ($exception->getMessage()) ? $exception->getMessage() : 'bad request';
                break;
            case 401:
                $title = $exception->getMessage();
                $detailedMessage = 'you are not authorized to access this method';
                break;
            case 409:
                $title = 'resource already exists';
                break;
            case 415:
                $title = 'unsupported content type – only accept application/json';
                break;
            case 422:
                $title = 'unprocessable entity';
                $detailedMessage = implode(' ', json_decode($exception->getMessage()));
                break;
        }

        if ($this->displayErrorDetails) {
            $html = '<p>The application could not run because of the following error:</p>';
            $html .= '<h2>Details</h2>';
            $html .= $this->renderHtmlException($exception);

            while ($exception = $exception->getPrevious()) {
                $html .= '<h2>Previous exception</h2>';
                $html .= $this->renderHtmlExceptionOrError($exception);
            }
        } else {
            $html = '<p>' . $title . '</p>';
        }

        $output = sprintf(
            "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>" .
            "<title>%s</title><style>body{margin:0;padding:30px;font:12px/1.5 Helvetica,Arial,Verdana," .
            "sans-serif;}h1{margin:0;font-size:48px;font-weight:normal;line-height:48px;}strong{" .
            "display:inline-block;width:65px;}</style></head><body><h1>%s</h1>%s</body></html>",
            $title,
            ($detailedMessage) ? $detailedMessage : $title,
            $html
        );

        return [$code, $output];
    }

}
